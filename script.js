var character = document.getElementById("character");
var block = document.getElementById("block");
var block2 = document.getElementById("block2");
var bird = document.getElementById("bird");
var characterDuck = document.getElementById("character-duck");
var counter = 0;
var startButton = document.getElementById("start");
startButton.onclick = check;

document.body.onkeyup = function (e) {
  if (e.keyCode == 32 || e.keyCode == 38) {
    jump();
  }
  if (e.keyCode == 40) {
    duck();
  }
};

function jump() {
  if (character.classList == "animate") {
    return;
  }
  character.classList.add("animate");
  setTimeout(function () {
    character.classList.remove("animate");
  }, 300);
}

function duck() {
  if (character.classList == "animateDuck") {
    return;
  }
  character.classList.add("animateDuck");
  setTimeout(function () {
    character.classList.remove("animateDuck");
  }, 500);
}

function stop() {
  block.style.animation = "none";
  block2.style.animation = "none";
  bird.style.animation = "none";
  alert("Game Over. score: " + Math.floor(counter / 100));
  window.location.reload();
}

function check() {
  block.classList.add("animate1");
  block2.classList.add("animate2");
  bird.classList.add("animateBird");
  startButton.disabled = true;
  setInterval(function () {
    let characterTop = parseInt(
      window.getComputedStyle(character).getPropertyValue("top")
    );

    let blockLeft = parseInt(
      window.getComputedStyle(block).getPropertyValue("left")
    );

    let block2Left = parseInt(
      window.getComputedStyle(block2).getPropertyValue("left")
    );

    let birdLeft = parseInt(
      window.getComputedStyle(bird).getPropertyValue("left")
    );

    if (blockLeft < 50 && blockLeft > -20 && characterTop >= 130) {
      stop();
    } else if (block2Left < 50 && block2Left > -20 && characterTop >= 130) {
      stop();
    } else if (birdLeft < 50 && birdLeft > -20 && characterTop <= 150) {
      stop();
    } else {
      counter++;
      document.getElementById("scoreSpan").innerHTML = Math.floor(
        counter / 100
      );
    }
  }, 10);
}
